
var AdventSlider = (function($,Backbone,_){

	function AdventSlider(el,options){
		var that = this;
		_.extend(this,Backbone.Events);

		that.wrapper = typeof el == 'object' ? el : document.getElementById(el);
		

		that.options = _.extend({
			duration : '300',
			easing : 'swing',
			node : 'div',
			width : 300,
			height : 300,
			maxIndex : 1
		},options);

		that.wrapper.style.position = 'relative';
		that.wrapper.style.height = this.options.height + 'px';

		that.slider = $(that.wrapper).children('div')[0];

		that.currentIndex = 0;
		that.maxIndex = that.options.items || that.slider.childNodes.length;

		$(that.slider).css({
			'overflow':'hidden',
			'height' : that.options.height,
			'width' : that.options.width * that.maxIndex,
			'margin-left' : that.options.width,
			'position' : 'absolute',
			'left' : '0px',
			'top' : '0px'

		});
		
	}

	AdventSlider.prototype.hasNext = function(){
		return this.currentIndex < this.maxIndex-1;
	};

	AdventSlider.prototype.hasPrev = function(){
		return this.currentIndex > 0;
	};


	AdventSlider.prototype.elementForItem = function(index){
		if(typeof index === 'undefined'){
			index = this.currentIndex;
		}
		return $(this.slider).children()[index];
	};
	AdventSlider.prototype.currentDOMElement = function(){
		return this.slider.childNodes[this.currentIndex];
	};

	AdventSlider.prototype.next = function(){
		if(this.hasNext()){
			this.goTo(this.currentIndex+1);
		}
	};

	AdventSlider.prototype.goTo = function(index,duration){
		var that = this;
		if(index > this.maxIndex || index < 0 || false && index === this.currentIndex) {
			return;
		}
		var slidePosition = this.options.width * (-index);
		this.trigger('preSlide', this.currentIndex);
		$(this.slider).animate({
			left: slidePosition
		}, {
			easing: this.options.easing,
			duration: typeof duration !== 'undefined' ? duration :this.options.duration,
			complete: function() {
				that.currentIndex = index;
				that.trigger('postSlide', that.currentIndex);
			}
		});
	};

	AdventSlider.prototype.prev = function(options){
		if(this.hasPrev()){
			this.goTo(this.currentIndex-1);
		}
		
	};

	return AdventSlider;

})(jQuery,Backbone,_);











