var util = {};
/*g.util.onTouch = function(element, callback, sensitivity) {
    if(!sensitivity) { sensitivity = 10; }

        var hasTouch = g.device.hasTouch;
        var START_EV = hasTouch ? 'touchstart' : 'mousedown', END_EV = hasTouch ? 'touchend' : 'mouseup';

    element.addEventListener(START_EV,function(e){
        e.preventDefault();
        if(e.changedTouches) {
                this.touch_y = e.changedTouches[0].pageY;
            }
    });

    element.addEventListener(END_EV,function(e){
        e.preventDefault();
        var movement = 0;
        if(e.changedTouches) {
                movement = Math.abs(this.touch_y - e.changedTouches[0].pageY);
            }
        if(movement < sensitivity){
            callback.call(this,e);
        }
    });

};*/

util.touchMe = function(element, start, move, end, cancel) {

        var hasTouch = true;//g.device.hasTouch;

        var addAllListeners = true;//g.device.isAndroid;

        var START_EV = hasTouch ? 'touchstart' : 'mousedown', MOVE_EV = hasTouch ? 'touchmove' : 'mousemove', END_EV = hasTouch ? 'touchend' : 'mouseup', CANCEL_EV = hasTouch ? 'touchcancel' : 'mouseup';

        var startX = 0;
        var startY = 0;

        var onStart = function(e) {

                var point = hasTouch ? e.touches[0] : e;
                startX = point.pageX;
                startY = point.pageY;

                // if havent already added them
                if (!addAllListeners) {
                        element.addEventListener(MOVE_EV, onMove);
                        element.addEventListener(END_EV, onEnd);
                        element.addEventListener(CANCEL_EV, cancel);
                }
                start.call(element, e, startX, startY);
        };

        var onMove = function(e) {

                var point = hasTouch ? e.touches[0] : e;
                move.call(element, e, point.pageX - startX, point.pageY - startY);

        };

        var onEnd = function(e) {
                if (!addAllListeners) {
                        element.removeEventListener(MOVE_EV, onMove);
                        element.removeEventListener(END_EV, onEnd);
                }
                end.call(element, e);

        };

        var onCancel = function(e) {
                if (!addAllListeners) {
                        element.removeEventListener(MOVE_EV, onMove);
                        element.removeEventListener(END_EV, onEnd);
                        element.removeEventListener(END_EV, CANCEL_EV, cancel);
                }
                cancel.call(element, e);
        };

        element.addEventListener(START_EV, onStart);

        if (addAllListeners) {
                element.addEventListener(MOVE_EV, onMove);
                element.addEventListener(END_EV, onEnd);
                element.addEventListener(CANCEL_EV, cancel);

        }

};

util.throwme = function(element, moveFn) {

        var options = {
                interval : 1000 / 60
        };
        var hasTouch = true;//g.device.hasTouch;
        var startPos = {
                x : 0,
                y : 0
        };
        var currentPos = {
                x : 0,
                y : 0
        };
        var timestamp = 0;
        var velocity = {
                x : 0,
                y : 0
        };
        var acceleration = {
                x : 0,
                y : 0
        };

        var timeoutRef = null;

        var stop = false;

        this.onStart = function(e) {

                stop = true;
                clearTimeout(timeoutRef);
                var point = hasTouch ? e.touches[0] : e;
                timestamp = Date.now();
                startPos.x = point.pageX;
                startPos.y = point.pageY;
                currentPos.x = 0;
                currentPos.y = 0;
                velocity.x = 0;
                velocity.y = 0;
        };

        this.onMove = function(e, x, y) {

                var time = Date.now();
                var point = hasTouch ? e.touches[0] : e;
                var distance = {
                        x : point.pageX - startPos.x - currentPos.x,
                        y : point.pageY - startPos.y - currentPos.y
                };
                var timestep = time - timestamp;

                velocity.x = distance.x / timestep;
                velocity.y = distance.y / timestep;

                //console.log('vX:' + velocity.x);

                timestamp = time;
                this.moveEl({
                        x : distance.x,
                        y : distance.y
                });

                currentPos.x += distance.x;
                currentPos.y += distance.y;
        };

        this.onEnd = function(e) {
                //console.log('VELOCITYX' + velocity.x);

                acceleration.x = -velocity.x / 400;
                //console.log('ACCEL' + acceleration.x);
                acceleration.y = -velocity.y / 100;
                stop = false;
                timeoutRef = setTimeout(this.onInterval, options.interval);

        };

        this.onInterval = function() {
                if (stop) {
                        return;
                }
                var time = Date.now();
                var timestep = time - timestamp;
                timestamp = time;

                var s = {};

                s.x = velocity.x * timestep + (acceleration.x * (timestep * timestep)) / 2;
                s.y = velocity.y * timestep + (acceleration.y * (timestep * timestep)) / 2;

                this.moveEl(s);
                this.accelerate(timestep);
                if (velocity.x === 0 && velocity.y === 0) {

                } else {
                        timeoutRef = setTimeout(this.onInterval, options.interval);
                }
        };

        this.accelerate = function(timestep) {
                var d = {
                        x : acceleration.x * timestep,
                        y : acceleration.y * timestep
                };
                if (Math.abs(d.x) >= Math.abs(velocity.x)) {
                        velocity.x = 0;
                } else {
                        velocity.x += d.x;
                }
                if (Math.abs(d.y) >= Math.abs(velocity.y)) {
                        velocity.y = 0;
                } else {
                        velocity.y += d.y;
                }
        };

        var movePos = {
                x : 0,
                y : 0
        };

        this.moveEl = function(d) {
                // console.log(movePos.x);
                movePos.x += d.x;
                movePos.y += d.y;
                // console.log(movePos.x);
                element.style.webkitTransform = 'translate3D(' + (movePos.x) + 'px,' + (movePos.y * 0) + 'px,0px)';

        };

        this.onEnd = _.bind(this.onEnd,this);
        this.onStart = _.bind(this.onStart,this);
        this.onMove = _.bind(this.onMove,this);
        // performance issues possilby?
        this.onInterval = _.bind(this.onInterval,this);

        this.moveEl = _.bind(this.moveEl,this);

        util.touchMe(element, this.onStart, this.onMove, this.onEnd, this.onEnd);

};

