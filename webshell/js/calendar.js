function adventResponse(response) {
    adventResponse.callbackFn(response['Response']);
}

adventResponse.callbackFn = null;

(function() {

    var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion);
    var isIphone = (/iphone/gi).test(navigator.appVersion);
    if(isIDevice) {
        $.getScript('http://danny.tw/advent/js/jquery.animate-enhanced.min.js');
        $.getScript('http://danny.tw/advent/js/ios-only.js?' + Date.now());
    }


    // settings
    var usingJSONP = false;
    var jsonpUrl = 'http://ecp-uat1.baplc.com/travel/advent-calendar/public/en_gb/itemid-json';
    var apiUrl = "http://danny.tw/ba/demo.json";
    var imgUrlPrexfix = "http://ecp-uat1.baplc.com/";
    var formUrl = "";
    var calendarWidth = 960;
    var calendarHeight = 370;
    var calendarDayWidth = calendarWidth / 3;
    var calendarDoorHeight = 252;
    var calendarDoorWidth = 218;
    var calendarDoorOpenTop = -225;
    var doorAnimationDuration = 400;
    var footerFadeDuration = 250;
    var popupFadeDuration = 300;
    var numberOfShutterImages = 6;
    var calendarBaseImagesLoaded = false;
    var calendarDataLoaded = false;
    var calendarDays = null;
    var calendarSlider = null;
    var initialCalendarIndex = 0;
    var calendarShown = false;
    var disableClicks = false;

    if(isIphone) {
        calendarWidth /= 2;
        calendarHeight /= 2;
        calendarDayWidth /= 2;
        calendarDoorWidth /= 2;
        calendarDoorHeight /= 2;
        calendarDoorOpenTop /= 2;
    }

    var imagesToPreload = ['img/window.png', 'img/lights.png', 'img/shutter.png', 'img/shutter0.png', 'img/shutter1.png', 'img/shutter2.png', 'img/shutter3.png', 'img/shutter4.png', 'img/shutter5.png'];
    for(var i = 1; i < 26; i++) {
        imagesToPreload.push('img/' + i + '.jpg');
    }
    //imagesToPreload,push('http://danny.tw/ba/large.jpg?' + Date.now() );
    // here we go......
    $(document).ready(

    function() {

        // debug, adding in a large image so there is some page load time
        // preload the images we require
        preload(imagesToPreload);
        $('#ba-advent-pageWrapper').waitForImages(function() {
            calendarBaseImagesLoaded = true;
            tryShowCalendar();

        }, function(loaded, count, success) {
            console.log(loaded + ' of ' + count + ' images has ' + (success ? 'loaded' : 'failed to load') + '.');
        }, true);


        var responseFn = function(data, textStatus, jqXHR) {


                var days = data.calendar;
                var date = new Date();
                // do we care about month?
                //var month = Date.getMonth()+1;
                var day = date.getDate();
                // day = 24;
                // see if we can find the day, and if so move to todays day
                for(var i = 0; i < days.length; i++) {
                    if(days[i].day == day) {
                        break;
                    }
                }


                // this is the index (day) to scroll to on start
                initialCalendarIndex = (i < days.length ? i : 0);
                // this is hopefully a bit of a hack to load the first image we will be going to first
                preload([days[initialCalendarIndex.img]]);

                setTimeout(function() {
                    initCalendar(data);
                }, 0);
            };

        if(usingJSONP) {
            adventResponse.callbackFn = responseFn;
            $.getScript(jsonpUrl);
        } else {
            // get the calendar details from the api
            $.getJSON(apiUrl, responseFn).error(function() {
                window.console.log('Error in get callback');
            });
        }

        $("#ba-advent-form-close").click(function() {
            hideForm();
        });
        $("#ba-advent-video-close").click(function() {
            hideVideo();
        });
    });

    function createHTML(days) {
        var cat = 0;
        var shutter = 0;
        var lastDay = 0;
        var slider = document.getElementById('ba-advent-slider');
        _.forEach(days, function(day) {
            var img = document.createElement('img');
            var imgNum = (day.day||lastDay);
            imgNum = imgNum >20 ? 20 : imgNum;
            img.src = 'img/offers/' + imgNum + '.jpg' ;// day.img ? imgUrlPrexfix + day.img : "http://corporaterecords.co.uk/cat/" + calendarDayWidth + '/' + calendarHeight + '?x=' + (++cat);
            img.className += ' ba-advent-day-image';
            var holder = document.createElement('div');
            $(holder).css({
                'float': 'left',
                'height': calendarHeight + 'px',
                'width': calendarDayWidth + 'px',
                'position': 'relative',
                'overflow': 'hidden'
            });
            var windowFrame = document.createElement('div');
            windowFrame.className += ' ba-advent-window-frame';
            var lights = document.createElement('div');
            lights.className += ' ba-advent-window-lights';

            var doorWrapper = document.createElement('div');
            doorWrapper.className += ' ba-advent-door-wrapper';


            var door = document.createElement('div');
            door.className += ' ba-advent-door';
            $(door).css({
                'height': calendarDoorHeight + 'px',
                'width': calendarDoorWidth + 'px',
                'background-image': "url('img/" + (day.day || lastDay) + ".jpg')"
            });

            lastDay = day.day ? Number(day.day) + 1 : lastDay + 1;
            shutter = ++shutter % numberOfShutterImages;

            var doorNumber = document.createElement('div');
            doorNumber.className += ' ba-advent-door-number';
            $(doorNumber).css({
                'background-image': "url('img/number" + day.day + "');"
            });


            door.appendChild(doorNumber);
            doorWrapper.appendChild(door);
            doorWrapper.appendChild(img);
            holder.appendChild(doorWrapper);
            holder.appendChild(windowFrame);
            holder.appendChild(lights);
            slider.appendChild(holder);

        });
    }

    function initCalendar(data) {
        var days = data.calendar;
        calendarDays = days;
        // setup the main header image and alt text
        $('#ba-advent-title').attr({
            "src": 'img/title.png' || data.headerImage,
            "alt": data.headerText
        });

        // set the form url
        formUrl = data.webformURL;

        createHTML(days);


        // create the slider
        calendarSlider = new BAAdventSlider(document.getElementById('ba-advent-carousel-wrapper'), {
            width: calendarDayWidth,
            height: calendarHeight,
            items: days.length
        });

        calendarSlider.on('postSlide', function(index) {
            disableClicks = false;

            // open the advent door that we just went to
            openDoor(calendarSlider.elementForItem(index));

            // fade the next and previous arrows in / out if need be
            $('#ba-advent-next')['fade' + (calendarSlider.hasNext() ? 'In' : 'Out')]();
            $('#ba-advent-prev')['fade' + (calendarSlider.hasPrev() ? 'In' : 'Out')]();

            var day = days[index];

            // setup the footer text to be that for the current day
            $('#ba-advent-day-headline').text(day.headline + ' ' + index);
            var text;
            switch(day.linkType) {
            case 'url':
                text = 'URL';
                break;
            case 'form':
                text = 'FORM';
                break;
            case 'video':
                text = 'VIDEO';
                break;
            case 'file':
                text = 'FILE';
                break;
            }

            $('#ba-advent-day-redeem').text(text).attr({
                'href': day.linkTarget
            });
            //$('#ba-advent-day-text').text(day.subheadline + ' ' + new Date());
            $('#ba-advent-day-wrapper').fadeIn(footerFadeDuration);


            // set the iframe url now, so hopefully it has loaded by the time it is shown ( if the user clicks redeem )
            var videoFrame = document.getElementById('ba-advent-video-iframe');
            var videoUrl = day.url || "http://www.youtube.com/embed/9bZkp7q19f0?rel=0";
            if(videoUrl && videoFrame.src != videoUrl) {
                videoFrame.src = videoUrl;
            }
        });

        calendarSlider.on('preSlide', function(index) {
            disableClicks = true;
            // close the current door and fade out the footer
            closeDoor(calendarSlider.elementForItem(index));
            $('#ba-advent-day-wrapper').fadeOut(footerFadeDuration);
        });

        $('#ba-advent-prev').click(function(event) {
            event.preventDefault();
            if(!disableClicks) {
                calendarSlider.prev();
            }
        });

        $('#ba-advent-next').click(function(event) {
            event.preventDefault();
            if(!disableClicks) {
                calendarSlider.next();
            }
        });

        $('#ba-advent-day-redeem').click(function(event) {

            event.preventDefault();

            if(!disableClicks) {
                var day = calendarDays[calendarSlider.currentIndex];
                switch(day.linkType) {
                case 'url':
                case 'file':
                    openUrl(day.linkTarget);
                    break;
                case 'form':
                    showForm();
                    break;
                case 'video':
                    showVideo();
                    break;

                }
            }

            return false;
        });


        $('#ba-advent-form').submit(function(event) {
            event.stopPropagation();
            event.preventDefault();
            var $inputs = $(this).find(':input');
            // get an associative array of just the values.
            var values = {};
            $inputs.each(function() {
                values[this.name] = $(this).val();
            });

            if(!validName(values['name'])) {
                alert('Invalid name');
                return;
            }

            if(!validSurname(values['surname'])) {
                alert('Invalid surname');
                return;
            }

            if(!validEmail(values['email'])) {
                alert('Invalid email');
                return;
            }


            if(!validAddress(values['address1'])) {
                alert('Invalid address');
                return;
            }


            if(!validTown(values['town'])) {
                alert('Invalid town');
                return;
            }

            if(!validPostcode(values['postcode'])) {
                alert('Invalid postcode');
                return;
            }

            if(!validCounty(values['county'])) {
                alert('Invalid county');
                return;
            }


            $.post(formUrl, values, function(data, textStatus, jqXHR) {
                alert('form submitted successfully');
            }).error(function() {
                window.console.log('error posting for data');
            });

            hideForm();
            return false;

        });
        $('#ba-advent-form-submit').click(function() {
            $('#ba-advent-form').trigger('submit');
        });


        $(document.body).keydown(function(e) {
            if(!disableClicks) {
                if(e.keyCode == 37) { // left
                    calendarSlider.prev();
                } else if(e.keyCode == 39) { // right
                    calendarSlider.next();
                }
            }
        });



        //make sure the window we are going to has had all the images loaded
        $(calendarSlider.elementForItem(initialCalendarIndex)).waitForImages(function() {
            calendarDataLoaded = true;
            tryShowCalendar();
        });



    }

    function tryShowCalendar(force) {
        if(force || (calendarDataLoaded && calendarBaseImagesLoaded && !calendarShown)) {
            var index = initialCalendarIndex;
            calendarShown = true;
            $('#ba-advent-calendar-wrapper').show();
            setTimeout(function() {
                $('#ba-advent-loading-wrapper').fadeOut(function() {
                    var duration = (index === 0) ? 0 : index < 1 ? 300 : index * 150;
                    calendarSlider.goTo(index, duration);
                });
            }, 100);
        }
    }

    function openDoor(el) {
        $(el).find('.ba-advent-day-image').css('display', 'block');
        if(isIDevice) {
            $(el).find('.ba-advent-door')[0].style.webkitTransform = 'translate3d(0,' + calendarDoorOpenTop + 'px,0)';
        } else {
            $(el).find('.ba-advent-door').animate({
                top: calendarDoorOpenTop + 'px'
            }, {
                easing: 'swing',
                duration: doorAnimationDuration,
                complete: function() {

                }
            });
        }
    }

    function closeDoor(el) {
        if(isIDevice) {
            $(el).find('.ba-advent-door')[0].style.webkitTransform = 'translate3d(0,0,0)';
            setTimeout(function() {
                $(el).find('.ba-advent-day-image').css('display', 'none');
            }, 450);
        } else {
            $(el).find('.ba-advent-door').animate({
                top: '0'
            }, {
                easing: 'swing',
                duration: doorAnimationDuration,
                complete: function() {
                    $(el).find('.ba-advent-day-image').css('display', 'none');
                }
            });
        }

    }


    function showForm() {
        var day = calendarDays[calendarSlider.currentIndex];
        $('#ba-advent-form-title').text(day.headline);
        //$('#ba-advent-form-subtitle').text(day.subheadline);
        $('#ba-advent-form-window,#ba-advent-popup-wrapper').fadeIn(popupFadeDuration);
    }

    function hideForm() {
        $('#ba-advent-form-window,#ba-advent-popup-wrapper').fadeOut(popupFadeDuration);
    }

    function showVideo(url) {
        $('#ba-advent-video-window,#ba-advent-popup-wrapper').fadeIn(popupFadeDuration);
    }

    function hideVideo() {

        var iframeSrcRemoved = false;
        $('#ba-advent-video-window,#ba-advent-popup-wrapper').fadeOut(popupFadeDuration, function(arg) {
            // kill any video thats playing....
            if(!iframeSrcRemoved) {
                var el = document.getElementById('ba-advent-video-iframe');
                var src = el.src;
                el.src = "";
                // set the src back again so it will load quickly if the user clicks the button again
                setTimeout(function() {
                    el.src = src;
                }, 100);
                iframeSrcRemoved = true;
            }

        });
    }

    function openUrl(url) {
        window.open(url, '_blank');
        window.focus();
    }


    function validEmail(email) {
        var validator = /^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if(!validator.test(email)) {
            return false;
        }
        return true;
    }

    function validName(name) {
        return name.length > 1;
    }

    function validSurname(name) {
        return name.length > 1;
    }


    function validAddress(str) {
        return str.length > 1;
    }

    function validTown(str) {
        return str.length > 1;
    }

    function validCounty(str) {
        return str.length > 1;
    }

    function validPostcode(str) {
        return str.length > 1;
    }

    function preload(arrayOfImages) {
        $(arrayOfImages).each(function() {
            $('<img />').attr('src', this).appendTo('#ba-advent-pageWrapper').css('display', 'none');
        });
    }

})();