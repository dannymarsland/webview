function NoClickDelay(el) {
    this.element = el;
    if(window.Touch) {
        this.element.addEventListener('touchstart', this, false);
    }
}

NoClickDelay.prototype = {
    handleEvent: function(e) {
        switch(e.type) {
        case 'touchstart':
            this.onTouchStart(e);
            break;
        case 'touchmove':
            this.onTouchMove(e);
            break;
        case 'touchend':
            this.onTouchEnd(e);
            break;
        }
    },

    onTouchStart: function(e) {
        e.preventDefault();
        this.moved = false;

        this.element.addEventListener('touchmove', this, false);
        this.element.addEventListener('touchend', this, false);
    },

    onTouchMove: function(e) {
        this.moved = true;
    },

    onTouchEnd: function(e) {
        this.element.removeEventListener('touchmove', this, false);
        this.element.removeEventListener('touchend', this, false);

        if(!this.moved) {
            // Place your code here or use the click simulation below
            var theTarget = document.elementFromPoint(e.changedTouches[0].clientX, e.changedTouches[0].clientY);
            if(theTarget.nodeType == 3) theTarget = theTarget.parentNode;

            var theEvent = document.createEvent('MouseEvents');
            theEvent.initEvent('click', true, true);
            theTarget.dispatchEvent(theEvent);
        }
    }
};


var loadCSS = function(filename) {
        var fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filename);
        document.body.appendChild(fileref);

    };

$(document).ready(function() {
    try {

        var isIphone = (/iphone/gi).test(navigator.appVersion);
        if(!isIphone) {
            var height = window.innerHeight ? window.innerHeight : screen.height;
            $('#ba-advent-pageWrapper').css({
                'top': (height - 600) / 2 + 'px'
            });
        }

        var tag = document.createElement('meta');
        tag.name = 'viewport';
        tag.content = "width=device-width, initial-scale=" + isIphone ? '1.0' : '1.07' + ", maximum-scale=" + isIphone ? '1.0' : '1.07' + ", user-scalable=0";
        if(isIphone) {
            tag.content = "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0";
        } else {
            tag.content = "width=device-width, initial-scale=1.07, maximum-scale=1.07, user-scalable=0";
        }

        document.getElementsByTagName('head')[0].appendChild(tag);

        tag = document.createElement('meta');
        tag.name = "apple-touch-fullscreen";
        tag.content = "YES";
        document.getElementsByTagName('head')[0].appendChild(tag);

        tag = document.createElement('meta');

        tag.name = "apple-mobile-web-app-capable";
        tag.content = "yes";
        document.getElementsByTagName('head')[0].appendChild(tag);

        loadCSS('css/ios-only.css?' + Date.now());
        if(isIphone) {
            loadCSS('css/iphone-only.css?' + Date.now());
        }
        setTimeout(function() {
            new NoClickDelay(document.getElementById('ba-advent-prev'));
            new NoClickDelay(document.getElementById('ba-advent-next'));
            new NoClickDelay(document.getElementById('ba-advent-day-redeem'));
        }, 100);
        document.body.addEventListener('touchstart', function(e) {
            e.preventDefault();
        });
        util.throwme($('#ba-advent-slider')[0],function(){});


    } catch(e) {
        alert(e);
    }
});