//
//  WebViewController.h
//  webview
//
//  Created by Danny Marsland on 08/11/2012.
//  Copyright (c) 2012 Danny Marsland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end
