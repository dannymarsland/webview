//
//  WebViewController.m
//  webview
//
//  Created by Danny Marsland on 08/11/2012.
//  Copyright (c) 2012 Danny Marsland. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.webView.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString* srcDir = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: @"webshell"];
	NSString* filePath = [srcDir stringByAppendingPathComponent: @"index.html"];
	NSString* url = [@"file://" stringByAppendingString:filePath];
    url = [url stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
	NSURL *reqURL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL: reqURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10 ];
    [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSLog(@"%@",request);
    return YES;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (NSUInteger) supportedInterfaceOrientations
{
    //Because your app is only landscape, your view controller for the view in your
    // popover needs to support only landscape
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

@end
