//
//  AppDelegate.h
//  webview
//
//  Created by Danny Marsland on 08/11/2012.
//  Copyright (c) 2012 Danny Marsland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
